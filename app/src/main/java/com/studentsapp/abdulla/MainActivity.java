package com.studentsapp.abdulla;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends CommonActivity {

    TextView toolbarText;
    List<ModelStudent> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private StudentListAdapter mAdapter;
    Dialog dialog;
    SwipeRefreshLayout swipeLayout;

    EditText name,gender,dob,address,postcode,studentNumber,courseTitle,startDate,bursary,email;
    Button add_btn,cancel_btn,edit_btn,delete_btn,save_button;
    LinearLayout add_cancel_layoout,update_delete_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        toolbarText = (TextView) findViewById(R.id.toolbar_text);
        if (toolbarText != null && toolbar != null) {
            toolbarText.setText(getTitle());
            setSupportActionBar(toolbar);
        }


        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getALLStudent();
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        swipeLayout.setRefreshing(false);
                        Toast.makeText(MainActivity.this, "Students Updated Successfully" , Toast.LENGTH_SHORT).show();
                    }
                }, 3000);

            }
        });

        swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);




    getALLStudent();



        list = new ArrayList<>();


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_students);
//                dialog.setTitle("Add Info");
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        name = (EditText) dialog.findViewById(R.id.name);
        gender = (EditText) dialog.findViewById(R.id.gender);
        dob = (EditText) dialog.findViewById(R.id.dob);
        address = (EditText) dialog.findViewById(R.id.address);
        postcode = (EditText) dialog.findViewById(R.id.postcode);
        studentNumber = (EditText) dialog.findViewById(R.id.studentNumber);
        courseTitle = (EditText) dialog.findViewById(R.id.courseTitle);
        startDate = (EditText) dialog.findViewById(R.id.startDate);
        bursary = (EditText) dialog.findViewById(R.id.bursary);
        email = (EditText) dialog.findViewById(R.id.email);
        add_btn = (Button) dialog.findViewById(R.id.add_btn);
        save_button = (Button) dialog.findViewById(R.id.save_button);
        cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        edit_btn = (Button) dialog.findViewById(R.id.edit_btn);
        delete_btn = (Button) dialog.findViewById(R.id.delete_btn);
        add_cancel_layoout = (LinearLayout) dialog.findViewById(R.id.add_cancel_layoout);
        update_delete_layout = (LinearLayout) dialog.findViewById(R.id.update_delete_layout);

        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ModelStudent modelStudent = new ModelStudent();
                modelStudent.setName(name.getText().toString());
                modelStudent.setGender(gender.getText().toString());
                modelStudent.setDob(dob.getText().toString());
                modelStudent.setAddress(address.getText().toString());
                modelStudent.setPostcode(Integer.valueOf(postcode.getText().toString()));
                modelStudent.setStudentNumber(Integer.valueOf(studentNumber.getText().toString()));
                modelStudent.setCourseTitle(courseTitle.getText().toString());
                modelStudent.setStartDate(startDate.getText().toString());
                modelStudent.setBursary(Integer.parseInt(bursary.getText().toString()));
                modelStudent.setEmail(email.getText().toString());
                addStudent(modelStudent);
                dialog.dismiss();
            }
        });


        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        delete_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteStudent(Integer.parseInt(studentNumber.getText().toString()));
                dialog.dismiss();
            }
        });
        edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save_button.setVisibility(View.VISIBLE);
                add_btn.setVisibility(View.GONE);
                update_delete_layout.setVisibility(View.GONE);
                add_cancel_layoout.setVisibility(View.VISIBLE);

                //

            }
        });

        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ModelStudent modelStudent = new ModelStudent();
                modelStudent.setName(name.getText().toString());
                modelStudent.setGender(gender.getText().toString());
                modelStudent.setDob(dob.getText().toString());
                modelStudent.setAddress(address.getText().toString());
                modelStudent.setPostcode(Integer.valueOf(postcode.getText().toString()));
                modelStudent.setStudentNumber(Integer.valueOf(studentNumber.getText().toString()));
                modelStudent.setCourseTitle(courseTitle.getText().toString());
                modelStudent.setStartDate(startDate.getText().toString());
                modelStudent.setBursary(Integer.parseInt(bursary.getText().toString()));
                modelStudent.setEmail(email.getText().toString());

                updateStudent(modelStudent);
                dialog.dismiss();
            }
        });


//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Do something after 100ms
        mAdapter = new StudentListAdapter(list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

                add_cancel_layoout.setVisibility(View.GONE);
                update_delete_layout.setVisibility(View.VISIBLE);


                ModelStudent modelStudent = new ModelStudent();
                modelStudent = list.get(position);
                name.setText(modelStudent.getName()+"");
                gender.setText(modelStudent.getGender()+"");
                dob.setText(modelStudent.getDob()+"");
                address.setText(modelStudent.getAddress()+"");
                postcode.setText(modelStudent.getPostcode()+"");
                studentNumber.setText(modelStudent.getStudentNumber()+"");
                courseTitle.setText(modelStudent.getCourseTitle()+"");
                startDate.setText(modelStudent.getStartDate()+"");
                bursary.setText(modelStudent.getBursary()+"");
                email.setText(modelStudent.getEmail()+"");

                dialog.show();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
//                        animation.start();
//            }
//        }, 5000);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                add_cancel_layoout.setVisibility(View.VISIBLE);
                update_delete_layout.setVisibility(View.GONE);
                add_btn.setVisibility(View.VISIBLE);
                save_button.setVisibility(View.GONE);

                name.setText("");
                gender.setText("");
                dob.setText("");
                address.setText("");
                postcode.setText("");
                studentNumber.setText("");
                courseTitle.setText("");
                startDate.setText("");
                bursary.setText("");
                email.setText("");

                dialog.show();


            }
        });


    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getALLStudent();
    }

    @Subscribe
    public void onEvent(MessageEvent event) {
        list = new ArrayList<>();
        list = event.getMessage();
        mAdapter = new StudentListAdapter(list);
        recyclerView.setAdapter(mAdapter);

    }
}
