package com.studentsapp.abdulla;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by abdulla on 2/27/18.
 */

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.MyViewHolder> {

    private List<ModelStudent> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name_tv, email_tv, id_tv;

        public MyViewHolder(View view) {
            super(view);
            name_tv = (TextView) view.findViewById(R.id.name_tv);
            email_tv = (TextView) view.findViewById(R.id.email_tv);
            id_tv = (TextView) view.findViewById(R.id.id_tv);

        }
    }


    public StudentListAdapter(List<ModelStudent> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ModelStudent modelStudent = list.get(position);

        holder.name_tv.setText(modelStudent.getName() + "");
        holder.email_tv.setText(modelStudent.getEmail() + "");
        holder.id_tv.setText(modelStudent.getStudentNumber() + "");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}