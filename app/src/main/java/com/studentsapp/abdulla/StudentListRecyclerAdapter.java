package com.studentsapp.abdulla;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by abdulla on 2/27/18.
 */

public class StudentListRecyclerAdapter extends ArrayAdapter<ModelStudent> {


    Context context;

    public StudentListRecyclerAdapter(Context context, int resourceId,
                                               List<ModelStudent> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;


        TextView name_tv,email_tv,id_tv;


    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        ModelStudent rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.student_list, null);
            holder = new ViewHolder();

            holder.name_tv = (TextView) convertView.findViewById(R.id.name_tv);
            holder.email_tv = (TextView) convertView.findViewById(R.id.email_tv);
            holder.id_tv = (TextView) convertView.findViewById(R.id.id_tv);


            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        // holder.serial.setText(position+1);


        holder.name_tv.setText(rowItem.getName()+"");
        holder.email_tv.setText(rowItem.getEmail()+"");
        holder.id_tv.setText(rowItem.getStudentNumber()+"");


        return convertView;
    }



}
