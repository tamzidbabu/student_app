package com.studentsapp.abdulla;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by abdulla on 2/27/18.
 */

public class CommonActivity extends AppCompatActivity{

    public void addStudent(ModelStudent modelStudent) {
        AsyncHttpClient client = new AsyncHttpClient();

        JSONObject object = new JSONObject();
        try {

            object.put("name", modelStudent.getName());
            object.put("gender", modelStudent.getGender());
            object.put("dob", modelStudent.getDob());
            object.put("address", modelStudent.getAddress());
            object.put("postcode", modelStudent.getPostcode());
            object.put("studentNumber", modelStudent.getStudentNumber());
            object.put("courseTitle", modelStudent.getCourseTitle());
            object.put("startDate", modelStudent.getStartDate());
            object.put("bursary", modelStudent.getBursary());
            object.put("email", modelStudent.getEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestParams params = new RequestParams();
        params.put("apikey", Constant.apikey);
        params.put("json", object);


        client.post(Constant.postStudentURL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                getALLStudent();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void updateStudent(ModelStudent modelStudent) {
        AsyncHttpClient client = new AsyncHttpClient();

        JSONObject object = new JSONObject();
        try {

            object.put("name", modelStudent.getName());
            object.put("gender", modelStudent.getGender());
            object.put("dob", modelStudent.getDob());
            object.put("address", modelStudent.getAddress());
            object.put("postcode", modelStudent.getPostcode());
            object.put("studentNumber", modelStudent.getStudentNumber());
            object.put("courseTitle", modelStudent.getCourseTitle());
            object.put("startDate", modelStudent.getStartDate());
            object.put("bursary", modelStudent.getBursary());
            object.put("email", modelStudent.getEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestParams params = new RequestParams();
        params.put("apikey", Constant.apikey);
        params.put("json", object);


        client.post(Constant.updateStudentURL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                getALLStudent();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }


    public void deleteStudent(int studentnumber) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("apikey", Constant.apikey);
        params.put("studentnumber", studentnumber);

        client.post(Constant.deleteStudent, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                getALLStudent();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    public void getALLStudent() {
        final List<ModelStudent> list = new ArrayList<>();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.getALLStudent, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);

                Log.i("ALL_STUDENTS",response.toString());

                for (int i = 0; i < response.length(); i++) {

                    try {
                        JSONObject object = response.getJSONObject(i);

                        ModelStudent modelStudent = new ModelStudent();
                        modelStudent.setName(object.getString("name"));
                        modelStudent.setGender(object.getString("gender"));
                        modelStudent.setDob(object.getString("dob"));
                        modelStudent.setAddress(object.getString("address"));
                        modelStudent.setPostcode(object.getInt("postcode"));
                        modelStudent.setStudentNumber(object.getInt("studentNumber"));
                        modelStudent.setCourseTitle(object.getString("courseTitle"));
                        modelStudent.setStartDate(object.getString("startDate"));
                        modelStudent.setBursary(object.getInt("bursary"));
                        modelStudent.setEmail(object.getString("email"));


                        list.add(modelStudent);
//                        adapter.notifyDataSetChanged();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

//                EventBus.getDefault().post(list);
                MessageEvent event = new MessageEvent(list);
                EventBus.getDefault().post(event);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });


    }
}
