package com.studentsapp.abdulla;

import java.util.List;

/**
 * Created by abdulla on 2/27/18.
 */

public class MessageEvent {
    public List<ModelStudent> mMessage;

    public MessageEvent(List<ModelStudent> message) {
        mMessage = message;
    }

    public List<ModelStudent> getMessage() {
        return mMessage;
    }

}
